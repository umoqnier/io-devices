# Previo 1: Introducción a memorias

**1. Mencione qué familias lógicas tipo transistor existen**
* TTL (Transistor-Transistor Logic)
* CMOS (Complementary Metal Oxide Semiconductor)
* ECL (Emitter Coupled Logic)
* NOMS, PMOS para circuitos de gran escala
* LVTTL (Low voltage TTL)
* LVCMOS (Low Voltage CMOS)
- https://www.academia.edu/6697685/FAMILIAS_L%C3%93GICAS._TTL_Transistor-Transistor_Logic?auto=download
- http://hyperphysics.phy-astr.gsu.edu/hbasees/Electronic/logfam.html  

**2. ¿Qué configuraciones de salida en tecnología TTL nos permiten conectar a las celdas de memoria de un bus común? Explique cada una de ellas**
- Colector abierto: An open collector is a common type of output found on many integrated circuits (IC), which behaves like a switch that is either connected to ground or disconnected. Instead of outputting a signal of a specific voltage or current, the output signal is applied to the base of an internal NPN transistor whose collector is externalized (open) on a pin of the IC. The emitter of the transistor is connected internally to the ground pin. If the output device is a MOSFET the output is called open drain and it functions in a similar way.
- Triestado: In digital electronics three-state, tri-state, or 3-state logic allows an output port to assume a high impedance state, effectively removing the output from the circuit, in addition to the 0 and 1 logic levels. This allows multiple circuits to share the same output line or lines (such as a bus which cannot listen to more than one device at a time).

**3. Explique ¿Qué es el Tiempo de propagación de la compuerta lógica y cómo se calcula?**
- Tiempo transcurrido entre un cambio de una señal de entrada y el cambio en la señal de salida. Se tienen dos valores para el tiempo de propagación que son los siguientes:
* tplh: Tiempo para cambiar de un valor bajo a uno alto
* tphl: Timepo para cambiar de un valor alto a uno bajo
El tiempo de propagación se calcula con la formula siguiente:
* Tpd: (tphl + tplh) ) / 2
- https://www.uv.es/~marinjl/electro/digital2.html

**4. Dibuje y explique el diagrama de bloques de una memoria**
- http://www1.frm.utn.edu.ar/arquitectura/unidad3.pdf

**5. Explique ¿Cómo funciona un decodificador y qué función tiene en una unidad de memoria?**
- Un decodificador funciona

La función de un decodificador en la memoria es la de procesar las direcciones y seleccionar una posición en la memoria
-http://www1.frm.utn.edu.ar/arquitectura/unidad3.pdf

**6. Realizar el dieño de un decodificador 3x8 con líneas de entrada en binario natural y salidas activada en nivel bajo (0), empleando solamente compuertas lógicas**

**7. ¿A cuántas entradas CMOS puede llevar una salida CMOS, de acuerdo a su FAN-OUT?**
50
