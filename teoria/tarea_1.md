

# Generaciones de computadoras

## Primera generación Tubos de vacío

### 1. ENIAC (Electonic Numerical Integrator and Computer)
Fue diseñana y construida en la universidad de Pensilvania. Propuesta por John Mauchly y su estudiante graduado John Eckert fue una computadora construida con _tubos de vacío_ y se programaba manualmente accionando interruptores y manipulando cables de plugging.
### 2. La maquina de Von Neumann
Programar la ENIAC era una tarea sumamente tediosa. El proceso de programación podía hacerse mas sencillo guardando en una memoria los datos. Implementa la idea del _storing-program concept_ propuesto por John von Neumann para una nueva computadora, la EDVAC (Electronic Discrete Variable Computer). Una computadora de este estilo constaba de las siguientes partes:
* Memoria principal que guardaba datos e instrucciones
* Una Unidad Aritmetico-Logica (ALU) que operaba con datos binarios
* Una Unidad Control que interpreta la instruccion y las ejecuta
* Entradas y Salidas (I/O) operadas por la unidad de control
#### Computadoras comerciales
* UNIVAC I y II
* UNIVAC 1100 series
* IMB 700/7000

## Segunda generación: Transistores

Se remplazan los tubos de vacío por transistores. El transistor es mas pequeño, barato y disipa menos calor que el tubo de vacío.El transistor esta hecho de silicon. Fue inventado en _Bell Labs_ en 1947. El uso de estos dispositivos marca la segunda generacion de computadoras. Esta definición esta determinada en gran medida por el hardware y esta en particular por la utilización de unidades aritméticas y de control mas complejas, uso de sistea operativo y lenguajes de alto nivel.
* __IMB 794__: Con esta computadora se ven diferentes usos de perifericos como memorias de 32 bits para las palabras y tiempos de acceso menores a 2 micro segundos. Una cosa destacable es la introducción del _data channel_ que es independiante del modulo de I/O. Otra caracterítica nueva es el uso de _multiplexores_ que accede a mwmoeia desde el CPU y el data channel permitiendo que estos dispositivos actuaran de forma independiente

## Tercera generación: Circuitos integrados
Un transistor único y autocontenido es llamado como _componente discreto_. Entre los 50s y 60s las computadoras funcionaban con elementos discretos indeendientes (como transistores, resistencias, capacitores, etc) contenidos en sus propios empaques. En 1958 con la invención del circuito integrado se definió la tercera gerenarión de computadoras. Probablemente los dos miembros mas importantes de esta generación son la __IMB System/360__ y la __DEC PDP-8__. Para esta arte de la computación surgieron las _small-scale integration (SSI)_

## Generaciónes futuras
No hay un concenso tan marcado para las definir las generaciones que estan despues de la tercera. Sin embargo, una referencia podrían ser las escalas de integración (LSI, VLSI y ULSI). Aunado a esto las aplicaciones desarrolladas despues de los 70s como la __memoria semiconductora__ y los __microprocesadores__ se tiene medianamente una idea del avance en el desarrollo de la computación.
