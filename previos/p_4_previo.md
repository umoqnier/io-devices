# Previo 4: Memorias direccionables por contenido (CAMs).

## 1. Explique ¿cuáles son las diferencias entre una memoria SRAM y una CAM?
Las memorias SRAM requieren de la dirección del dato al que se quiere llegar. Por otra parte, las memorias CAM no necesitan de la dirección si no que es suficiente con darle cierta información que es la llave o clave.
## 2. Explique las ventajas que presenta la operación de búsqueda en una memoria CAM
Como las CAM realizan la búsqueda de forma simultanea en todas las localidades se observa una clara ventaja al tener tiempos de busqueda instantaneos y a continuación efectuar lecturas/escrituras propias de las RAM comúnes. Esta condición es posible gracias a que la búsqueda se hace por hardware.
## 3. Explique ¿qué se requiere para llegar a la información en las memorias RAM yROM?
Se requiere conocer la dirección del dato al que se quiere llegar y un decodificador para seleccionar dichas direcciones. La dirección ingresada en el decodificador retornará la información guardada en ella.
## 4. Explique ahora ¿cómo se puede llegar a una información específica en una memoria CAM?
A la CAM se le debe proporcionar la información que se desea buscar. Se efectuará la busqueda en toda la memoria, de forma simultanea, y si la información se encuentra la CAM retornará una lista de una o más direcciones donde fue encontrada la información. En algunas implementaciones se retornará tambien el contenido de dichas localidades.

-[CAM](https://en.wikipedia.org/wiki/Content-addressable_memory)

## 5. Identifique y explique los diferentes bloques en que debe estar integrada la memoria CAM presentada en la figura 7 de la práctica.
## 6. Probar el buen funcionamiento de la celda CAM a nivel compuertas lógicas, mostrada en la figura 2, realice varias operaciones de lectura, escritura y comparaciones.
## 7. Explique ¿cómo se llevarían a cabo las operaciones de lectura, escritura y comparación en la celda CAM a nivel transistor?, mostrada en la figura 3.
## 8. Explique a detalle ¿cómo se llevarían a cabo las operaciones de lectura, escritura y búsquedas en la memoria CAM?, mostrada en la figura 7.
