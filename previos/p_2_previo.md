#

## 1. Existen 5 tipos de memoria ROM, mencione ¿cuáles son, sus características y su definición principal?

  1. **ROM de mascara (MROM):** Las memoria de mascara son las menos caras de las memorias de estado solido. Son usualmente utilizadas para guardar software de videojuegos, datos fijos de equipos electricos, fuentes para impresoras laser, diccionarios en procesadores de palabras y bancos de sonido en instrumentos musicales electricos. Algunos metodos para programar una MROM son los siguientes:
    1. Contacto metalico para conectar un transistos a la linea de bits
    2. implante de canal para crear un transistor en modo de mejora o en modo de agotamiento
    3. Oxido de compuerta fina o gruesa, la cual crea ya sea un transisto estandar o un transistor de alto umbral, respectivamente.
  2. **ROM programable con electricidad (EPROM):** Las memorias EPROM son progamable solamente una vez y jamas pueden ser borradas. Usualmente son para usos especificos.
  3. **ROM programable y borrable con luz ultravioleta (EPROM/UVEPROM):** Son un tipo de ROM progamable con electricidad y borrable con rayos ultravioleta. Usualmente se deja 20 minutos en el aparato para desprogamarla completamente y puede ser reprogramada de 6 a 8 veces.
  4. **ROM programable y borrable electricamente (EEPROM):** Ofrecen capacidades y desempeño altos. Se requiere solo una fuente de poder externa para prgramar o borrar la memoria. Escritura y borrado con operaciones que se efectuan Byte por Byte.

  https://web.eecs.umich.edu/~prabal/teaching/eecs373-f10/readings/rom-eprom-eeprom-technology.pdf
  

## 2. Realice el diseño y simulación en computadora del inciso (i) de la práctica a partir del uso del circuito integrado 74LS139.

## 3. Si una memoria tiene una organización de 32 K x 8 bits, calcular:

  1. La capacidad de la memoria en celdas o bits: *262144 bits*
  2. El tamaño de la matriz de almacenamiento suponiendo que la memoria tenga dos decodificadores, de qué tamaño serían éstos.  *Dos decodificadores de 8x512*
  3. El número de líneas de dirección *16 para direccion*
  4. El número de líneas de datos. *8 para datos*
  5. Mencione el número de líneas de habilitación y de control que deberá tener. *??*

## 4. Describir las características de la EPROM que utilizará en el inciso (ii) de la práctica, para implementar un decodificador BCD de 7 segmentos. Dibujar el circuito eléctrico completo con las entradas y las salidas que deberá alambrar y presentar la tabla de datos y direcciones a grabar en su EPROM de acuerdo a las características que se piden en el mismo.
