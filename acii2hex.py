#!/usr/bin/python

DB = {'0': 'C0',
            '1': 'F9',
            '2': 'A4',
            '3': 'B0',
            '4': '99',
            '5': '92',
            '6': '82',
            '7': 'F8',
            '8': '80',
            '9': '98',
            'a': '88',
            'b': '83',
            'c': 'C6',
            'd': 'A1',
            'e': '86',
            'f': '8E',
            'g': '98',
            'h': '89',
            'i': 'F9',
            'l': 'C7',
            'n': 'C8',
            'o': 'A3',
            'p': '8C',
            'q': '98',
            'r': 'CE',
            's': '92',
            't': '87',
            'u': 'C1',
            '-': 'BF',
            ' ': 'FF',
            'y': '99'}


def convert(phrase):
    aux = ''
    cou = 1
    for i in phrase:
        if cou % 16 == 0:
            aux += '{0}\n'.format(DB[i])
        else:
            aux += '{0} '.format(DB[i])
        cou += 1
    return aux


if __name__ == '__main__':
    aux = 'dispositiuos de alnnacenanniento de entrada y salida -- sennestre 2019-1 -- barriga nnartines diego 311033607 -- luis alberto  311211931 -- grupo 3 -- una se borra con lus y otra con electricidad -- hola que hace'
    print(convert(aux))
