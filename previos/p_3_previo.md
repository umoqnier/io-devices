# Previo 3: Memorias de lectura-escritura semiconductoras (RAMs)

## 1. Explique la diferencia entre una memoria RAM estática (SRAM) y una memoria RAM dinámica (DRAM).

Las SRAM son un tipo de memorias rápidas que requieren de energía para mantener el contenido. Son usadas en los registros de alta velocidad, cache y memorias de poca capacidad como el buffer de una tarjeta gráfica. Está memoria es más rápida porque utiliza circuitos flip-flops que ditribuyen la corriente en una dirección (1 o 0).

Las DRAM son volatiles ya que solo utilizan un transistor y un capacitor por cada bit a guardar. Es mucho más barata que las SRAM. Los capacitores de las DRAM deben ser constantemente recargados para no  erder la información.

https://www.pcmag.com/encyclopedia/term/42192/dynamic-ram
https://www.pcmag.com/encyclopedia/term/52041/static-ram

## 2. Explique la diferencia entre una memoria de acceso secuencial (SAM) y una memoria de acceso aleatorio (RAM).
Con las memorias de acceso secuencial se tiene que pasar por n-1 localidades de memoria antes de llegar a la localidad n deseada. Esto implica que el tiempo de acceso es lineal.

Las memorias de acceso aleatorio tienen la característica de tener un tiempo de acceso inmediato ya que no es necesario pasar por n-1 localidades para llegar a la localidad n deseasa. Se dice que el tiempo de acceso es constante. Regularmente se utilizan decodificadores para este tipo de memorias.

## 3. Algunos circuitos integrados RAM tienen terminales de entrada/salida comunes, ¿Qué circuito nos permite lograr esto y explique cómo lo hace?
https://electrouni.files.wordpress.com/2010/08/salidas-logicas-triestados.pdf

## 4. Simule con MAXPLUS II de Altera o algún otro simulador, la memoria RAM estática con una organización de 2 x 2 que se presenta en el diagrama esquemático (abajo) donde analizaremos el comportamiento de las entradas y salidas. Nota: Deberá entregar la simulación y el análisis de tiempo para todas las posibles entradas, en papel al instructor.
![circuito](p_3_c.jpeg)
![simulacion](sim.jpeg)
## 5. Simule en forma digital ¿cómo se implementaría un habilitador general CE (Chip Enable)? de la figura anterior, para la práctica será necesario su armado, así que contemple comprar los componentes necesarios para su implementación y funcionamiento.
